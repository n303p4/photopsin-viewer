"""Various helper functions."""

import ipaddress


def validate_ip_address(address):
    try:
        return bool(ipaddress.ip_address(address))
    except Exception:
        return False
