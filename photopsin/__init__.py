"""
Photopsin viewer

This program allows you to view the status of multiple machines running folding@home, all at once.

Requires Photopsin to be installed and running on folding@home machines.
"""

from datetime import datetime
import csv
from io import StringIO
import json
import os
from traceback import print_exc

from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, jsonify, make_response, request, render_template
import requests
try:
    import webview
except ImportError:
    webview = None

from . import config_manager
from . import constants
from . import helpers


def get_folding_stats(ip_addresses: list[str], folding_slots: list[list], progress_data: list[list]):
    donors = []
    for address in ip_addresses:
        try:
            response = requests.get(f"http://{address}/local-stats.json", timeout=2)
            if response.status_code >= 400:
                continue
            data = response.json()
            if data:
                donors.append(data)
            else:
                donors.append([])
        except Exception:
            donors.append([])
            print_exc()
    while folding_slots:
        folding_slots.pop()
    while progress_data:
        progress_data.pop()
    now = datetime.utcnow()
    for index, donor_info in enumerate(donors):
        if not donor_info:
            continue
        for slot_info in donor_info.get("slot_info", []):
            queue_info = slot_info.get("queue_info", {})
            simulation_info = slot_info.get("simulation_info", {})
            hardware_info = slot_info.get("hardware_info", {})

            donor_desc = []
            slot_id = slot_info.get("id")
            if slot_id:
                donor_desc.append(f"Slot {slot_id} of")
            donor = donor_info.get("donor")
            if donor:
                donor_desc.append(donor)
            donor_desc.append(f"({ip_addresses[index]})")

            description = slot_info.get("description")
            if not hardware_info and "gpu" in description:
                description = "GPU is lost; system reboot required!"

            project = simulation_info.get("project")
            run = simulation_info.get("run")
            clone = simulation_info.get("clone")
            gen = simulation_info.get("gen")
            has_prcg = any((project, run, clone, gen))
            if has_prcg:
                prcg = (
                    f"{project} ({run}, {clone}, {gen})",
                    f"https://stats.foldingathome.org/project/{project}"
                )
            else:
                prcg = None

            percentdone = queue_info.get("percentdone") if has_prcg else None
            ppd = commas(queue_info.get("ppd"))
            if ppd == "0":
                ppd = None
            tpf = queue_info.get("tpf")

            deadline = queue_info.get("deadline")
            if deadline:
                try:
                    deadline = datetime.fromisoformat(deadline.rstrip("Z")) - now
                    deadline = str(deadline).rsplit(":", 1)[0]
                except Exception:
                    pass

            utilization = hardware_info.get("utilization")
            if isinstance(utilization, (int, float)):
                utilization = f"{utilization}%"
            load_average = " ".join("{:.2f}".format(l) for l in hardware_info.get("load_average", []))
            clock = hardware_info.get("clock")
            if isinstance(clock, list):
                clock = [
                    f"{int(round(sum(clock) / len(clock)))}MHz",
                    "\n".join("Processor {0}: {1:.3f}MHz".format(i, c) for i, c in enumerate(clock))
                ]
                if load_average:
                    clock[1] = f"Load average: {load_average}\n{clock[1]}"
            if isinstance(clock, (int, float)):
                clock = f"{int(round(clock))}MHz"
            memory_usage = hardware_info.get("memory_usage")
            memory_total = hardware_info.get("memory_total")
            if isinstance(memory_usage, int):
                memory_usage = f"{memory_usage}MiB ({memory_usage * 100 // (memory_total or 1)}%)"
            if isinstance(memory_total, int):
                memory_total = f"{memory_total}MiB"
            fan_speed = hardware_info.get("fan_speed")
            temperature = hardware_info.get("temperature", "")
            if isinstance(temperature, list):
                temperature = [f"{temperature[0][1]}°C", "\n".join(f"{t[0]}: {t[1]}°C" for t in temperature)]
            elif isinstance(temperature, int):
                temperature = f"{temperature}°C"
            power_usage = hardware_info.get("power_usage")
            power_usage = f"{power_usage // 1000}W" if power_usage else None
            power_limit = hardware_info.get("power_limit")
            power_limit = f"{power_limit // 1000}W" if power_limit else None

            folding_slots.append([
                (" ".join(donor_desc), description),
                (prcg, simulation_info.get("core")),
                (percentdone, slot_info.get("status")),
                (ppd, tpf),
                (queue_info.get("eta"), deadline),
                (commas(queue_info.get("creditestimate")), commas(queue_info.get("basecredit"))),
                (utilization, clock),
                (memory_usage, memory_total),
                (f"{fan_speed}%" if isinstance(fan_speed, int) else None, temperature),
                (power_usage, power_limit)
            ])
            progress_data.append([
                float(percentdone.rstrip("%")) if percentdone else None,
                progress_per_second(fah_time_to_seconds(tpf)) if tpf else None,
                simulation_info.get("eta")
            ])


def commas(number: int):
    if str(number).isdecimal():
        number = int(str(number))
    if isinstance(number, int):
        return f"{number:,}"
    return None


def fah_time_to_seconds(timestring: str):
    substrings = timestring.split(" ")
    seconds = 0
    for index, substring in enumerate(substrings):
        if substring.replace(".", "", 1).isdecimal():
            substring_as_float = float(substring)
            substrings[index] = substring_as_float
        else:
            if substring == "days":
                seconds += substrings[index-1] * 24 * 60 * 60
            elif substring == "hours":
                seconds += substrings[index-1] * 24 * 60
            elif substring == "mins":
                seconds += substrings[index-1] * 60
            else:
                seconds += substrings[index-1]
    return seconds


def progress_per_second(time_per_frame, total_iterations: int = 100):
    return (100 / (total_iterations or 100)) / time_per_frame


def create_app(ip_addresses: list[str] = None, use_csrf_token: bool = False,
               resizable: bool = False, app_name: str = None):
    """Application factory."""

    if not isinstance(ip_addresses, list):
        ip_addresses = []

    app = Flask(__name__, static_folder="img", template_folder="templates")
    app.secret_key = os.urandom(64)

    columns = {
        ("Slot | Donor", "Description"): (0, 0),
        ("Work unit PRCG", "Core"): (9, 6),
        (r"% done", "Status"): (3.5, 4.5),
        ("PPD", "TPF"): (4.5, 6),
        ("ETA", "Deadline"): (6.5, 6.5),
        ("Est. credit", "Base credit"): (4, 4),
        ("Util.", "Clock"): (3, 4),
        ("Memory usage", "Total memory"): (6.5, 5.5),
        ("Fan sp.", "Temp."): (3, 3),
        ("Pwrusg.", "Pwrlim."): (3, 3)
    }
    folding_slots = []
    progress_data = []

    get_folding_stats(ip_addresses, folding_slots, progress_data)

    scheduler = BackgroundScheduler()
    scheduler.add_job(
        func=get_folding_stats,
        args=(ip_addresses, folding_slots, progress_data),
        trigger="cron",
        second="*/10"
    )
    scheduler.start()

    @app.route("/")
    def index():
        """Index route. Displays all the shiny numbers."""
        invert_colors = "invert" in request.args or "invertcolors" in request.args
        font_contrast = request.args.get("fontcontrast", "high").lower()
        unstack_rows = "unstackrows" in request.args
        if unstack_rows:
            column_names_ = []
            for names in columns.keys():
                column_names_ += list(names)
            column_widths = columns.values()
        else:
            column_names_ = columns.keys()
            column_widths = [max(i) for i in columns.values()]
        no_progress = "noprogress" in request.args
        if no_progress:
            use_javascript = False
            no_flash = True
        else:
            use_javascript = "nojs" not in request.args
            no_flash = "noflash" in request.args
        progress_as_json = None
        if use_javascript:
            progress_as_json = json.dumps(progress_data)
        hide_columns = filter(
            lambda i: i.isdecimal(), [c.strip() for c in request.args.get("hidecols", "").split(",")]
        )
        hide_rows = filter(
            lambda i: i.isdecimal(), [r.strip() for r in request.args.get("hiderows", "").split(",")]
        )
        hide_donor = "hidedonor" in request.args
        hide_paused_slots = "hidepaused" in request.args
        hide_disabled_slots = "hidedisabled" in request.args
        resizable_ = resizable or "resizable" in request.args or "resizeable" in request.args
        hide_header = "hideheader" in request.args
        font_size = None
        _font_size = request.args.get("fontsize", "")
        if _font_size.isdecimal():
            font_size = _font_size
        return render_template(
            "index.jinja",
            app_name=app_name,
            csrf_token=webview.token if webview and use_csrf_token else None,
            column_names=column_names_,
            column_widths=column_widths,
            hide_columns=hide_columns,
            hide_rows=hide_rows,
            folding_slots=folding_slots,
            use_javascript=use_javascript,
            no_progress=no_progress,
            no_flash=no_flash,
            progress_as_json=progress_as_json,
            hide_paused_slots=hide_paused_slots,
            hide_disabled_slots=hide_disabled_slots,
            hide_donor=hide_donor,
            resizable=resizable_,
            hide_header=hide_header,
            unstack_rows=unstack_rows,
            invert_colors=invert_colors,
            font_contrast=font_contrast,
            font_size=font_size
        )

    return app


if __name__ == "__main__":
    app = create_app()
    app.run()
