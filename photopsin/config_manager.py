"""Config manager for Photopsin viewer."""

import json
import tkinter


class ConfigManager:

    def __init__(self, filepath: str):
        self.filepath = filepath
        self.config = {}
        self.temp = {}

    def load(self):
        try:
            with open(self.filepath) as file_object:
                self.config = json.load(file_object)
        except Exception:
            pass

    def save(self):
        try:
            with open(self.filepath, "w") as file_object:
                json.dump(self.config, file_object)
        except Exception:
            pass

    def get(self, key, default = None):
        if str(key).startswith("_"):
            return self.temp.get(key, default)
        return self.config.get(key, default)

    def set(self, key, value):
        if str(key).startswith("_"):
            self.temp[key] = value
        else:
            self.config[key] = value

    def has(self, key):
        if str(key).startswith("_"):
            return key in self.temp
        return key in self.config

    def clear(self):
        self.config = {}

    def from_dict(self, dict_: dict):
        for key, value in dict_.items():
            if isinstance(value, tkinter.Variable):
                self.set(key, value.get())
            else:
                self.set(key, value)
