"""Photopsin viewer constants."""

APP_NAME = "photopsin_viewer"
PID_FILE = ".photopsin_viewer"
READABLE_NAME = "Photopsin viewer"
DEFAULT_CONFIG = {
    "invert": 0,
    "fontcontrast": "high",
    "fontsize": "16",
    "noflash": 0,
    "resizable": 0,
    "unstackrows": 0,
    "hideheader": 0,
    "hidedonor": 0,
    "hidepaused": 0,
    "hidedisabled": 0,
    "hidecols": "",
    "hiderows": ""
}
WELCOME_MESSAGE = (
    R"Welcome! You can set your preferences in this control panel.\n\n"
    "It can be opened again at any time by clicking the gear button in "
    "the top left of the stats window."
)
