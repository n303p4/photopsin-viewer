"""Start Photopsin viewer as a standalone application."""

from functools import wraps
import json
import os
import sys
import threading
import tkinter
from tkinter import messagebox, simpledialog
from tkinter import ttk
from urllib.parse import urlencode

from flask import abort, request
from pid import PidFile
from plyer import notification
import xerox
import webview

from photopsin import create_app
from photopsin.config_manager import ConfigManager
from photopsin.constants import APP_NAME, PID_FILE, READABLE_NAME, DEFAULT_CONFIG, WELCOME_MESSAGE
from photopsin.helpers import validate_ip_address


def validate_csrf_token(function):
    @wraps(function)
    def wrapper(*args, **kwargs):
        token = request.headers.get("X-CSRF-Token")
        if token != webview.token:
            abort(400)
        return function(*args, **kwargs)
    return wrapper


def load_settings(config_manager: ConfigManager, window: webview.Window = None, *, default_config: dict = None):
    if not default_config:
        default_config = DEFAULT_CONFIG

    if not config_manager.has("_base_url"):
        config_manager.set("_base_url", window.get_current_url())
    base_url = config_manager.get("_base_url")

    query = {}
    for key, default in default_config.items():
        value = config_manager.get(key, default)
        if isinstance(value, int) and value:
            query[key] = ""
        elif isinstance(value, str):
            query[key] = value
    window.load_url(f"{base_url}?{urlencode(query)}")


def create_control_panel(config_manager: ConfigManager, window: webview.Window = None,
                         *, default_config: dict = None):
    """Create a control panel for the program."""

    if not default_config:
        default_config = DEFAULT_CONFIG

    root = tkinter.Tk()
    root.title(f"{READABLE_NAME} - Control panel")
    root.resizable(False, False)

    control_variables = {}
    for key, default in default_config.items():
        if isinstance(default, int):
            control_variables[key] = tkinter.IntVar()
        elif isinstance(default, str):
            control_variables[key] = tkinter.StringVar()
        control_variables[key].set(config_manager.get(key, default))

    def apply_settings():
        config_manager.from_dict(control_variables)
        config_manager.set("firstrun", True)
        config_manager.save()
        load_settings(config_manager, window)

    def clear_settings():
        if not messagebox.askyesno(READABLE_NAME, "This change is irreversible! Are you sure?"):
            return
        config_manager.clear()
        config_manager.save()
        load_settings(config_manager, window)
        for key, default in default_config.items():
            control_variables[key].set(config_manager.get(key, default))

    frame = tkinter.Frame(root)
    frame.grid(padx=5, pady=5)

    tkinter.Label(frame, text="Invert colors:").grid(row=0, column=0, sticky=tkinter.W)
    ttk.Checkbutton(frame, variable=control_variables["invert"]).grid(row=0, column=1, sticky=tkinter.W)

    tkinter.Label(frame, text="Font contrast:").grid(row=1, column=0, sticky=tkinter.W)
    ttk.Radiobutton(frame, variable=control_variables["fontcontrast"], value="low").grid(row=1, column=1, sticky=tkinter.W)
    tkinter.Label(frame, text="Low").grid(row=1, column=2, sticky=tkinter.W)
    ttk.Radiobutton(frame, variable=control_variables["fontcontrast"], value="med").grid(row=1, column=3, sticky=tkinter.W)
    tkinter.Label(frame, text="Med").grid(row=1, column=4, sticky=tkinter.W)
    ttk.Radiobutton(frame, variable=control_variables["fontcontrast"], value="high").grid(row=1, column=5, sticky=tkinter.W)
    tkinter.Label(frame, text="High").grid(row=1, column=6, sticky=tkinter.W)

    tkinter.Label(frame, text="Font size:").grid(row=2, column=0, sticky=tkinter.W)
    ttk.Combobox(
        frame, textvariable=control_variables["fontsize"], value=[*range(8, 25), 28, 32, 48, 72]
    ).grid(row=2, column=1, columnspan=6, sticky=tkinter.W)

    tkinter.Label(frame, text="Disable update flash:").grid(row=3, column=0, sticky=tkinter.W)
    ttk.Checkbutton(frame, variable=control_variables["noflash"]).grid(row=3, column=1, sticky=tkinter.W)

    tkinter.Label(frame, text="Resize table to fit window:").grid(row=4, column=0, sticky=tkinter.W)
    ttk.Checkbutton(frame, variable=control_variables["resizable"]).grid(row=4, column=1, sticky=tkinter.W)

    tkinter.Label(frame, text="Unstack rows:").grid(row=5, column=0, sticky=tkinter.W)
    ttk.Checkbutton(frame, variable=control_variables["unstackrows"]).grid(row=5, column=1, sticky=tkinter.W)

    tkinter.Label(frame, text="Hide header:").grid(row=6, column=0, sticky=tkinter.W)
    ttk.Checkbutton(frame, variable=control_variables["hideheader"]).grid(row=6, column=1, sticky=tkinter.W)

    tkinter.Label(frame, text="Hide donor:").grid(row=7, column=0, sticky=tkinter.W)
    ttk.Checkbutton(frame, variable=control_variables["hidedonor"]).grid(row=7, column=1, sticky=tkinter.W)

    tkinter.Label(frame, text="Hide paused slots:").grid(row=8, column=0, sticky=tkinter.W)
    ttk.Checkbutton(frame, variable=control_variables["hidepaused"]).grid(row=8, column=1, sticky=tkinter.W)

    tkinter.Label(frame, text="Hide disabled slots:").grid(row=9, column=0, sticky=tkinter.W)
    ttk.Checkbutton(frame, variable=control_variables["hidedisabled"]).grid(row=9, column=1, sticky=tkinter.W)

    tkinter.Label(frame, text="Hide columns:").grid(row=10, column=0, sticky=tkinter.W)
    ttk.Entry(frame, textvariable=control_variables["hidecols"]).grid(row=10, column=1, columnspan=6, sticky=tkinter.W)

    tkinter.Label(frame, text="Hide rows:").grid(row=11, column=0, sticky=tkinter.W)
    ttk.Entry(frame, textvariable=control_variables["hiderows"]).grid(row=11, column=1, columnspan=6, sticky=tkinter.W)

    hide_label = tkinter.Label(frame, text="Columns & rows are 1-indexed & comma-separated.")
    hide_label.grid(row=12, column=0, columnspan=7, sticky=tkinter.W)

    button_frame = tkinter.Frame(frame)
    button_frame.grid(row=13, column=0, columnspan=7)

    ttk.Button(button_frame, text="Reset to defaults", command=clear_settings).pack(side=tkinter.LEFT, padx=2)
    ttk.Button(button_frame, text="Apply", command=apply_settings).pack(side=tkinter.LEFT, padx=2)

    config_manager.set("_cp_open", True)

    def on_close():
        config_manager.set("_cp_open", False)
        root.destroy()

    root.protocol("WM_DELETE_WINDOW", on_close)
    root.mainloop()


def main(ip_addresses):
    fullscreen = "-f" in ip_addresses or "--fullscreen" in ip_addresses
    if fullscreen:
        while "-f" in ip_addresses:
            ip_addresses.remove("-f")
        while "--fullscreen" in ip_addresses:
            ip_addresses.remove("--fullscreen")

    if not ip_addresses:
        ip_addresses = str(simpledialog.askstring(READABLE_NAME, "Enter IP addresses here (comma-separated):")).split(",")
    ip_addresses = list(filter(validate_ip_address, [a.strip() for a in ip_addresses]))
    if not ip_addresses:
        messagebox.showerror(READABLE_NAME, "No valid IP addresses were specified. Halting.")
        sys.exit(1)

    config_manager = ConfigManager(os.path.join(os.path.expanduser("~"), f".{APP_NAME}.json"))
    config_manager.load()
    config_manager.set("_cp_open", False)
    server = create_app(ip_addresses=ip_addresses, use_csrf_token=True, app_name=READABLE_NAME)
    window = webview.create_window(
        f"{READABLE_NAME} - Folding@home stats",
        server,
        width=1,
        height=1,
        fullscreen=fullscreen
    )

    @server.route("/open-control-panel")
    @validate_csrf_token
    def open_control_panel():
        if config_manager.get("_cp_open"):
            return "", 304
        try:
            control_panel_thread = threading.Thread(target=create_control_panel, args=(config_manager, window))
            control_panel_thread.start()
        except Exception:
            abort(500)
        return "", 200

    @server.route("/toggle-fullscreen", methods=["GET"])
    @validate_csrf_token
    def toggle_fullscreen():
        try:
            window.toggle_fullscreen()
        except Exception:
            abort(400)
        return "", 200

    @server.route("/copy-url-to-clipboard", methods=["GET"])
    @validate_csrf_token
    def copy_url_to_clipboard():
        try:
            xerox.copy(window.get_current_url())
        except Exception:
            notification.notify(READABLE_NAME, "An error occurred while copying URL to clipboard.")
            abort(400)
        else:
            notification.notify(READABLE_NAME, "Copied URL to clipboard.")
        return "", 200

    @server.route("/resize-window", methods=["POST"])
    @validate_csrf_token
    def resize_window():
        try:
            data = json.loads(request.data)
            width = data.get("width")
            height = data.get("height")
            if window.width != width or window.height != height:
                window.resize(width, height)
                return "", 200
            else:
                return "", 304
        except Exception:
            abort(400)

    @server.route("/alert-dialog", methods=["POST"])
    @validate_csrf_token
    def alert_dialog():
        try:
            data = json.loads(request.data)
            messagebox_thread = threading.Thread(target=messagebox.showinfo,
                                                 args=(READABLE_NAME, data["message"]))
            messagebox_thread.start()
        except Exception:
            abort(400)
        return "", 200

    if config_manager.get("firstrun"):
        webview.start(func=lambda: load_settings(config_manager, window))
    else:
        config_manager.set("firstrun", True)
        config_manager.save()
        webview.start(
            func=lambda: window.evaluate_js(f"openControlPanel().then(() => {{ alertDialog(`{WELCOME_MESSAGE}`) }});")
        )


if __name__ == "__main__":
    with PidFile(PID_FILE, piddir=os.path.expanduser("~")):
        main(sys.argv[1:])
