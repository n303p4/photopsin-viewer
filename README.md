# Photopsin viewer

A Photopsin client that supports viewing stats from multiple machines all at once.

![Preview image of Photopsin viewer](preview.png)

## Quickstart

Install Photopsin on any FAH machines you want to monitor, and ensure that it is working correctly.

On the machine where you want to view FAH stats, run `python3 viewer.py <list of IP addresses>` (e.g. `python3 viewer.py 192.168.1.20 192.168.1.21`).

If no valid IP addresses are provided, Photopsin viewer will request them via a dialog. If none are valid after that, it will halt.

Alternatively, you can substitute `viewer.py` for `server.py`, which is an HTTP server intended for local use. You may use your own browser with it this way, by pointing it to http://localhost:8090. This is useful for e.g. using it as an OBS widget.

Photopsin viewer's display output can be adjusted using either the control panel in `viewer.py` mode, or by adding URL query parameters in `server.py` mode.

Query parameters for `server.py` mode are as follows:

* `nojs` - Disables JavaScript. This simplifies the appearance of Photopsin viewer, and disables progress and ETA extrapolation between fetches of Photopsin data. This option is not exposed in `viewer.py`.
* `noflash` - By default, the progress bars will flash when fetching data. This option disables that.
* `noprogress` - Disables both JavaScript and update flashes, and also replaces `<progress>` elements with `<div>` elements of appropriate width. This allows outdated and/or embedded browsers to still view progress. Not exposed in `viewer.py`.
* `invert` or `invertcolors` - Invert display colors.
* `fontcontrast` - Adjust font color contrast between `low`, `med`/`medium`, and `high`. Defaults to `high`.
* `fontsize` - Set the font size. Default size is 16px.
* `resizable` or `resizeable` - Resize table to fit the screen area.
* `unstackrows` - Rows are shown in one line rather than two lines.
* `hideheader` - Do not show table header/column names.
* `hidedonor` - Hide donor information.
* `hidepaused` - Hide paused slots.
* `hidedisabled` - Hide disabled slots.
* `hidecols` - Hide columns. Columns are 1-indexed and separated by commas; e.g. `hidecols=1,2` hides the first two columns.
* `hiderows` - Hide columns. Rows are like columns.

Example URL: http://localhost:8090/?hidepaused&hidedisabled&hidedonor&hidecols=2,6,7,8,9,10
