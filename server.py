"""Start Photopsin viewer as a web server, for use in your own browser."""

import os
import sys

from pid import PidFile

from photopsin import create_app
from photopsin.constants import APP_NAME, PID_FILE, READABLE_NAME
from photopsin.helpers import validate_ip_address


def main(ip_addresses):
    if not ip_addresses:
        ip_addresses = input("Enter IP addresses here (space-separated): ").split()
    ip_addresses = list(filter(validate_ip_address, ip_addresses))
    if not ip_addresses:
        print("No valid IP addresses were specified. Halting.")
        sys.exit(1)
    allow_from_network = input("Allow connections from network? (y/N): ").lower() == "y"
    app = create_app(ip_addresses=ip_addresses, app_name=READABLE_NAME)
    if allow_from_network:
        app.run(host="0.0.0.0", port=8090)
    else:
        app.run(port=8090)


if __name__ == "__main__":
    with PidFile(PID_FILE, piddir=os.path.expanduser("~")):
        main(sys.argv[1:])
